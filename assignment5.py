#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify, Response
import random
import json

app = Flask(__name__)

# Dictionary of books
books = [{'title': 'Software Engineering', 'id': '1'}, 
		 {'title': 'Algorithm Design', 'id':'2'}, 
		 {'title': 'Python', 'id':'3'}]

next_id = 4

@app.route('/book/JSON/')
def bookJSON():
    r = json.dumps(books)
    return Response(r)

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books = books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    global next_id
    if request.method == 'POST':
        book_title = request.form['newTitle']
        books.append({'title': book_title, 'id': str(next_id)})
        next_id += 1
        return redirect(url_for('showBook'))
    return render_template('newBook.html')

def update_name(new_name, book_id):
    for book in books:
        if book['id'] == str(book_id):
            book['title'] = new_name
            break
    return

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    if request.method == 'POST':
        new_name = request.form['replaceWith']
        if (new_name != ''):
            update_name(new_name, book_id)
        return redirect(url_for('showBook'))
    else:
        single_book = {}
        for book in books:
            if int(book['id']) == book_id:
                single_book = book
        return render_template('editBook.html', book = single_book)

@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    global books
    if request.method == 'POST':
        books = list(filter(lambda book: book['id'] != str(book_id), books))
        return redirect(url_for('showBook'))
    else:
        single_book = {}
        for book in books:
            if int(book['id']) == book_id:
                single_book = book
        return render_template('deleteBook.html', book = single_book)

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 1234)